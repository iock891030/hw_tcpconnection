/*
A very simple TCP server written in Go.
*/
package main

import (
	"fmt"
	"io/ioutil"
	"net"
	"sort"
	"strconv"
	"strings"
)

const (
	addr = "127.0.0.1"
	port = 8000
)

func main() {

	src := addr + ":" + strconv.Itoa(port)
	listener, err := net.Listen("tcp", src)
	if err != nil {
		fmt.Println(err.Error())
	}
	defer listener.Close()
	fmt.Printf("TCP server start and listening on %s.\n", src)

	for {
		conn, err := listener.Accept()
		if err != nil {
			fmt.Printf("Some connection error: %s\n", err)
		}

		go handleConnection(conn)
	}
}

func handleConnection(conn net.Conn) {
	remoteAddr := conn.RemoteAddr().String()
	fmt.Println("Client connected from: " + remoteAddr)
	valid := false
	// Make a buffer to hold incoming data.
	buf := make([]byte, 1024)
	for {
		// Read the incoming connection into the buffer.
		reqLen, err := conn.Read(buf)
		if err != nil {

			if err.Error() == "EOF" {
				fmt.Println("Disconned from ", remoteAddr)
				break
			} else {
				fmt.Println("Error reading:", err.Error())
				break
			}
		} else {
			input := strings.Split(string(buf[:reqLen]), " ")
			if input[1] == "LOGIN" {
				if valid {
					conn.Write([]byte("Permission denied"))
				} else if LOGIN(input[0]) {
					conn.Write([]byte("valid"))
					valid = true
				} else {
					conn.Write([]byte("invalid"))
				}
			} else if input[1] == "SLOGIN" {
				if valid {
					conn.Write([]byte("Permission denied"))
				} else if SLOGIN(input[0]) {
					conn.Write([]byte("valid"))
					valid = true
				} else {
					conn.Write([]byte("invalid"))
				}

			} else if valid {
				if input[len(input)-1] == "SPO" {
					for _, ele := range input {
						if ele != "SPO" {
							sides := strings.Split(ele, ",")
							var data []int
							for _, eleInSides := range sides {
								a, _ := strconv.Atoi(eleInSides)
								data = append(data, a)
							}
							sort.Slice(data, func(i, j int) bool {
								if data[i] < data[j] {
									return true
								}
								return false
							})
							output := SPO(data[0], data[1], data[2])
							conn.Write([]byte("Output: " + output))
						}
					}
				} else {
					conn.Write([]byte("Output:WTF BRO"))
				}
			} else {
				conn.Write([]byte("Permission denied"))
			}
		}
		// Send a response back to person contacting us.

		fmt.Printf("len: %d, recv: %s\n", reqLen, string(buf[:reqLen]))
	}
	// Close the connection when you're done with it.
	//conn.Close()
}
func SPO(a, b, c int) string {
	fmt.Println(a, b, c)
	if a+b <= c {
		return "Not a triangle"
	}
	if a == b && b == c {
		return "perfect triangle"
	}
	if a*a+b*b > c*c {
		return "Acute triangle"
	}
	if a*a+b*b == c*c {
		return "Right triangle"
	}
	if a*a+b*b < c*c {
		return "obtuse angle"
	}
	return "3thing rum in SPO"
}
func LOGIN(_auth string) bool {
	auths, err := ioutil.ReadFile("loginTable.txt")
	auth := strings.Split(_auth, ":")
	fmt.Println("LOGIN req:  " + auth[0] + ":" + auth[1] + " ?")
	if err != nil {
		fmt.Println(err)
	} else {
		authsArr := strings.Split(string(auths), "\r\n")
		for _, ele := range authsArr {
			et := strings.Split(ele, ", ")

			if strings.EqualFold(auth[0], et[0]) {
				if strings.EqualFold(auth[1], et[1]) {
					return true
				}
				return false
			}
		}
	}
	return false
}
func SLOGIN(auth string) bool {
	id := strings.Split(auth, ":")
	plaintext := id[0] + ", " + CSC(id[1], -2)
	fmt.Println("SLOGIN req:  " + plaintext)
	auths, err := ioutil.ReadFile("loginTable.txt")
	if err != nil {
		fmt.Println(err)
	} else {
		authsArr := strings.Split(string(auths), "\r\n")
		for _, ele := range authsArr {
			if strings.EqualFold(plaintext, ele) {
				return true
			}
		}
	}
	return false
}

func CSC(text string, direction int) string {
	// shift -> number of letters to move to right or left
	// offset -> size of the alphabet, in this case the plain ASCII
	offset := rune(95)
	runes := []rune(text)
	forward := true
	if direction < 0 {
		forward = false
		direction = -direction
	}
	shift := rune(direction)
	for i := 0; i < len(runes); i++ {
		if !forward {
			if runes[i] >= ' '+shift && runes[i] <= '~' {
				runes[i] = runes[i] - shift
			} else if runes[i] >= ' ' && runes[i] < ' '+shift {
				runes[i] = runes[i] - shift + offset
			}
		} else {
			if runes[i] >= ' ' && runes[i] <= '~'-shift {
				runes[i] = runes[i] + shift
			} else if runes[i] > ' '-shift && runes[i] <= '~' {
				runes[i] = runes[i] + shift - offset
			}
		}
	}
	return string(runes)
}
