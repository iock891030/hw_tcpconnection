package main

import (
	"fmt"
	"net"
	"strings"

	//"text/scanner"
	"bufio"
	"io/ioutil"
	"os"
)

func main() {
	inputType := "nope"
	conn, err := net.Dial("tcp", "127.0.0.1:8000")
	go func() {
		if err != nil {
			fmt.Println(err.Error())
		}
	}()

	for {
		scanner := bufio.NewScanner(os.Stdin)
		fmt.Printf("Input:")
		scanner.Scan()
		text := scanner.Text()
		if strings.Contains(text, " SPO") {
			inputType = "SPO"
			if strings.Contains(text, ".txt") {
				inputType = "textSpo"
			}
		} else if strings.Contains(text, " LOGIN") {
			inputType = "LOGIN"
		} else if strings.Contains(text, " SLOGIN") {
			inputType = "SLOGIN"
		} else {
			inputType = "Err"
		}
		if inputType == "LOGIN" {
			res, err := contiTCP(conn, text)
			if err != nil {
				fmt.Println(err.Error())
			} else {
				fmt.Println("Output: " + res)
			}
		} else if inputType == "SLOGIN" {
			d := strings.Split(text, " ")
			auth := strings.Split(d[0], ":")
			cypher := CSC(auth[1], 2)
			//fmt.Println("Input:" + auth[0] + ":" + cypher + " SLOGIN")
			res, err := contiTCP(conn, auth[0]+":"+cypher+" SLOGIN")
			if err != nil {
				fmt.Println(err.Error())
			} else {
				fmt.Println("Output: " + res)
			}

		} else if inputType == "SPO" {
			res, err := contiTCP(conn, text)
			if err != nil {
				fmt.Println(err.Error())
			} else {
				fmt.Println(res)
			}
		} else if inputType == "textSpo" {
			fileAdr := strings.Split(text, " ")
			file, err := ioutil.ReadFile(fileAdr[0])
			if err != nil {
				fmt.Println(err.Error())
			} else {
				data := strings.Split(string(file), "\r\n")
				for _, e := range data {
					d := e + " SPO"
					fmt.Println(d + ":")
					res, err := contiTCP(conn, d)
					if err != nil {
						fmt.Println(err.Error())
					} else {
						fmt.Println(res)
					}
				}

			}
		} else {
			fmt.Println("Output: input Err")
		}

	}
}
func contiTCP(conn net.Conn, msg string) (string, error) {
	// send to socket
	conn.Write([]byte(msg))

	// listen for reply
	bs := make([]byte, 1024)
	len, err := conn.Read(bs)
	if err != nil {
		return "", err
	} else {
		return string(bs[:len]), err
	}
}

func CSC(text string, direction int) string {
	// shift -> number of letters to move to right or left
	// offset -> size of the alphabet, in this case the plain ASCII
	offset := rune(95)
	runes := []rune(text)
	forward := true
	if direction < 0 {
		forward = false
		direction = -direction
	}
	shift := rune(direction)
	for i := 0; i < len(runes); i++ {
		if !forward {
			if runes[i] >= ' '+shift && runes[i] <= '~' {
				runes[i] = runes[i] - shift
			} else if runes[i] >= ' ' && runes[i] < ' '+shift {
				runes[i] = runes[i] - shift + offset
			}
		} else {
			if runes[i] >= ' ' && runes[i] <= '~'-shift {
				runes[i] = runes[i] + shift
			} else if runes[i] > ' '-shift && runes[i] <= '~' {
				runes[i] = runes[i] + shift - offset
			}
		}
	}
	return string(runes)
}
